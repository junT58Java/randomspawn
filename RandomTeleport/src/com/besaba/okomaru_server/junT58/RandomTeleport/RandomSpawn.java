/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.besaba.okomaru_server.junT58.RandomTeleport;

import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

/**
 *
 * @author junT58
 */
public class RandomSpawn implements Listener{

    private Main plugin;

    public RandomSpawn(){
        plugin = Main.getInstance();
    }

    public void telep(CommandSender sender){
        if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.RED + "ゲーム内から実行してください");
            return;
        }
        List<String> loclist = plugin.getConfig().getStringList("Spawnlocationlist");
        int locsec = new Random().nextInt(loclist.size());
        String locstr = loclist.get(locsec);
        //x-y-z-world
        String[] locka = locstr.split("_");
        int x;
        int y;
        int z;
        try {
        	x = Integer.parseInt(locka[0]);
        	y = Integer.parseInt(locka[1]);
        	z = Integer.parseInt(locka[2]);
        } catch(NumberFormatException ex){
        	ex.printStackTrace();
        	return;
        }
        World world = plugin.getServer().getWorld(locka[3]);
        Location loc = new Location(world, x, y, z);
        ((Player) sender).teleport(loc);
        sender.sendMessage(ChatColor.RED + "テレポートしました。");
    }

    public void setresp(String[] args, CommandSender sender){
        if(!sender.hasPermission("randomteleport.set")){
            sender.sendMessage(ChatColor.RED + "権限がありません。");
            return;
        }
        if(!(sender instanceof Player)){
            sender.sendMessage(ChatColor.RED + "ゲーム内から実行してください");
            return;
        }
        Player send = (Player) sender;
        String saveloc = send.getLocation().getBlockX() + "_" + send.getLocation().getBlockY() + "_" + send.getLocation().getBlockZ() + "_" + send.getWorld().getName();
        List<String> list = plugin.getConfig().getStringList("Spawnlocationlist");
        list.add(saveloc);
        plugin.getConfig().set("Spawnlocationlist", list);
        plugin.saveConfig();
        sender.sendMessage(ChatColor.GREEN + "追加しました。");
    }

}
