package com.besaba.okomaru_server.junT58.RandomTeleport;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{

	private static Main instance;

	public static Main getInstance(){
		return instance;
	}

	public void onEnable(){
		instance = this;
	}

	public boolean onCommand(org.bukkit.command.CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args){
		if(cmd.getName().equalsIgnoreCase("randomspawn")){
			new RandomSpawn().telep(sender);
		}else if(cmd.getName().equalsIgnoreCase("addpoint")){
			new RandomSpawn().setresp(args, sender);
		}
		return true;
	}

}
